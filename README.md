# MÉTODO EXPLÍCITO DE EULER: ESCRITA COM A LINGUAGEM PYTHON

Este é um métodos considerados mais simples, ele consiste em fazer a aproximação de uma curva atráves da sua derivada. Ou seja, por meio da tangente de uma função solução de uma EDO, conseguindo assim se aproximar da solução por meio de valores $`y_1`$

$`y_1=y_0+h\:g(x_0,\:y_0)`$

Na qual $g$ é a função da EDO

$` f(x,\:y) = g(x,\:y) `$

---

## PROBLEMA PROPOSTO

Traçar uma curva aproximada á EDO abaixo, na qual um dos pontos se encontra em $`(0, 1)`$

$`f'(x,\:y) = x + y`$

---
